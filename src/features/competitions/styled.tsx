import styled from 'styled-components';
import { device } from '@main/__data__/constants';

const calcResponsive = (min, max) =>
    `calc(${min}px + (${max} - ${min}) * ((100vw - 768px) / (1440 - 768)))`;

export const Wrapper = styled.section`
    display: flex;
    flex-direction: column;
    width: 100%;
    display: flex;
    flex-direction: column;
    align-self: center;
    padding: 0 20px;
    justify-self: center;
    position: relative;
    margin-bottom: 80px;

    @media (min-width: 1440px) {
        padding: 0;
    }
`;

export const CompetitionsTitle = styled.div`
    font-size: 22px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.43;
    letter-spacing: normal;
    text-align: center;
    color: #1e1d20;
    margin-bottom: 20px;

    @media ${device.tablet} {
        font-size: 50px;
    }
`;

export const CompetiotionBlock = styled.div`
    position: relative;
    display: flex;
    width: 100%;
    justify-content: space-around;
    align-self: center;
    justify-self: center;

    > div {
        text-align: center;

        &:nth-child(n+3) {
            display: none;
        }
        @media ${device.tablet} {
            &:nth-child(n+3) {
                display: block;
            }
        }
    }

    :nth-child(2):before {
        content: "";
        position: absolute;
        bottom: 8px;
        /* left: -20px; */
        height: 1px;
        background-color: #003eff;
        width: 100%;
        max-width: 1400px;
        min-width: 768px;

        @media (min-width: 1440px) {
            width: 100%;
            max-width: 1440px;
            min-width: 768px;
            left: 0;
        }

        @media ${device.tablet} {
            bottom: 12px;
        }
    }
`;

export const CompetiotionDate = styled.div`
    font-size: 38px;
    font-weight: bold;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #1e1d20;

    @media ${device.tablet} {
        font-size: ${calcResponsive(80 * 0.82, 80)};
    }
`;

export const CompetiotionMonth = styled.div`
    font-size: 22px;
    font-weight: 600;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;
    color: #1e1d20;

    @media ${device.tablet} {
        font-size: ${calcResponsive(32 * 0.82, 32)};
    }
`;

export const CompetiotionName = styled.a`
  font-size: ${calcResponsive(24 * 0.65, 24)};
  font-weight: bold;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #1e1d20;
  text-decoration: none;
  max-width: 188px;

  &:hover,
  &:visited {
    text-decoration: none;
    color: #1e1d20;
  }

    &:nth-child(n+3) {
        display: none;
    }
    @media ${device.tablet} {
        &:nth-child(n+3) {
            display: block;
        }
    }
`;

export const Circle = styled.div`
    width: 16px;
    height: 16px;
    border-radius: 50%;
    background-color: #003eff;
    margin: 0 auto;

    @media ${device.tablet} {
        width: 24px;
        height: 24px;
    }
`;
