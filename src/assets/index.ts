import logo from './icons/logo.svg';
import logoGrey from './icons/logo_grey.svg';
import phone from './icons/phone.svg';
import phoneGrey from './icons/phone_grey.svg';
import bigLogo from './icons/bigLogo.svg';

import { ArrowInSirlce,
    Arrow, ArrowDown, Edit, Save, Cancel, NoImagePlaceHolder, PlayBtn, ButovoLogo, PDFIcon, PhotoStub,
    WreathIcon, AvaFem, UDSImage, Logo, MenuIcon, FilterIcon, LikeIcon, Error, Error404 } from './svgIcons';
import facebook from './icons/social/facebook.svg';
import instagram from './icons/social/instagram.svg';
import ok from './icons/social/odnoklassniki.svg';
import twitter from './icons/social/twitter.svg';
import vk from './icons/social/vk.svg';
import g_facebook from './icons/social/gray/g-facebook.svg';
import g_instagram from './icons/social/gray/g-instagram.svg';
import g_ok from './icons/social/gray/g-odnoklassniki.svg';
import g_twitter from './icons/social/gray/g-twitter.svg';
import g_vk from './icons/social/gray/g-vk.svg';

const social = {
    icons: {
        facebook,
        instagram,
        ok,
        twitter,
        vk,
    },
    gray: {
        g_facebook,
        g_instagram,
        g_ok,
        g_twitter,
        g_vk,
    }
};

export { Arrow, bigLogo, logo, logoGrey, phone, phoneGrey, ArrowInSirlce, NoImagePlaceHolder, PlayBtn,
    WreathIcon,

    ArrowDown,

    ButovoLogo,
    PDFIcon,
    PhotoStub,
    AvaFem,
    UDSImage,
    Logo,
    MenuIcon,
    social,
    Edit,
    Save,
    Cancel,
    FilterIcon,
    LikeIcon,
    Error,
    Error404,
};
