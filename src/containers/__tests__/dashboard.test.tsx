import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import * as Thunk from 'redux-thunk';
import moxios from 'moxios';
import { act } from 'react-dom/test-utils';

import reducer from '@main/__data__/reducers';

import Dashboard from '../dashboard';

configure({ adapter: new Adapter() });

describe('mount all app', () => {
    beforeEach(() => {
        moxios.install();
        window.__webpack_public_path__ = '/';
    });

    afterEach(() => moxios.uninstall());

    it('mount', async () => {
        const app = mount(
            <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
                <Dashboard />
            </Provider>
        );

        expect(app).toMatchSnapshot();

        await moxios.wait(jest.fn);

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 200,
                response: require('../../../stubs/api/getMainData')
            });
        });

        app.update();
        expect(app).toMatchSnapshot();
    });

    it('show error on error', async () => {
        const app = mount(
            <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
                <Dashboard />
            </Provider>
        );

        await moxios.wait(jest.fn);

        await act(async () => {
            const request = moxios.requests.mostRecent();
            await request.respondWith({
                status: 404
            });
        });

        app.update();
        expect(app).toMatchSnapshot();
    });
});
