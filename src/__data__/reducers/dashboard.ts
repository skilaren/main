import { MAIN_DATA_FETCH_SUCCESS, MAIN_DATA_FETCH, MAIN_DATA_FETCH_FAIL } from '../../__data__/constants/actions-types';
import { MainData, AsyncDataState, MainDataRequest } from '../model/interfaces';
import { AnyAction } from 'redux';

type MainDataState = Partial<MainDataRequest> & AsyncDataState<MainData>;

const initialState: MainDataState = {
  data: null,
  loading: false,
  error: null
};

const fetchHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
  ...state,
  loading: true,
});

const fethSuccessHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
  ...state,
  data: action.data!.mainData,
  loading: false,
});

const fetchErrorHandler = (state: MainDataState, action: AnyAction): MainDataState => ({
  ...state,
  data: null,
  loading: false,
  error: action.error || true
});

const handlers = {
  [MAIN_DATA_FETCH]: fetchHandler,
  [MAIN_DATA_FETCH_SUCCESS]: fethSuccessHandler,
  [MAIN_DATA_FETCH_FAIL]: fetchErrorHandler
};

export const dashboard = (state: MainDataState = initialState, action: AnyAction) =>
  Object.prototype.hasOwnProperty.call(handlers, action.type) ? handlers[action.type](state, action) : state;
